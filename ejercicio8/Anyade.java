
import java.io.*;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class Anyade {
    public static void agrega(Set<String> motos) {
        File dhFile = new File("motosfiltradas.csv");
        try (FileWriter fw = new FileWriter(dhFile); BufferedWriter bw = new BufferedWriter(fw);) {
            for (String line : motos) {
                bw.write(line);
                bw.newLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}