import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class Motos {
	public static void main(String[] args) {
        if (args.length ==0){
            System.out.println("Marca no especificada");
            return;
        }
		File fin= new File("motos.csv");
        //List<String> motos = new ArrayList<String>();
        Set<String> motos = new TreeSet<String>();
		try (	InputStreamReader fr = new InputStreamReader( new FileInputStream(fin), "UTF8");
				BufferedReader br = new BufferedReader(fr);
				) {
			String linea;
			do {
                linea = br.readLine();
                if (linea!=null) {
                    if (linea.toLowerCase().contains(args[0])){
                    motos.add(linea);                    
                }
                }
			} while (linea!=null);
		} catch (Exception e) {
			e.printStackTrace();
        }         
        Anyade.agrega(motos);
        for (String provincia : motos) {
            System.out.println(provincia);
        }
	}
}

