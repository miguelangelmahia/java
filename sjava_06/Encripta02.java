public class Encripta02 {
    public static String Encripta01(String dato) {
        String letranumero = "";

        for (int i = 0; i < dato.length(); i++) {
            char cambiado = dato.charAt(i);
            switch (cambiado) {
            case 'a': {
                cambiado = '1';
                break;
            }
            case 'e': {
                cambiado = '2';
                break;
            }
            case 'i': {
                cambiado = '3';
                break;
            }
            case 'o': {
                cambiado = '4';
                break;
            }
            case 'u': {
                cambiado = '5';
                break;
            }
            default:
                break;
            }
            letranumero = letranumero + cambiado;
        }
        return letranumero;
    }

    public static String Encripta02(String dato) {
        String letranumero = "";

        for (int i = 0; i < dato.length(); i++) {
            char cambiado = dato.charAt(i);
            if (cambiado == 'z') {
                cambiado = 'a';
                letranumero = letranumero + cambiado;

            } else {
                cambiado++;
                letranumero = letranumero + cambiado;
            }
        }
        return letranumero;
    }

    public static String Desencripta01(String dato) {
        String letranumero = "";

        for (int i = 0; i < dato.length(); i++) {
            char cambiado = dato.charAt(i);
            switch (cambiado) {
            case '1': {
                cambiado = 'a';
                break;
            }
            case '2': {
                cambiado = 'e';
                break;
            }
            case '3': {
                cambiado = 'i';
                break;
            }
            case '4': {
                cambiado = 'o';
                break;
            }
            case '5': {
                cambiado = 'u';
                break;
            }
            default:
                break;
            }
            letranumero = letranumero + cambiado;
        }
        return letranumero;
    }

    public static String Desencripta02(String dato) {
        String letranumero = "";

        for (int i = 0; i < dato.length(); i++) {
            char cambiado = dato.charAt(i);
            if (cambiado == 'a') {
                cambiado = 'z';
                letranumero = letranumero + cambiado;

            } else {
                cambiado--;
                letranumero = letranumero + cambiado;
            }
        }
        return letranumero;
    }
}