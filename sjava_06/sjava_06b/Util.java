import java.util.Random;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class Util {

    public static String Meses(int mes, String idioma) {
        String mesescrito;

        if (mes == 1 && idioma == "es") {
            mesescrito = "Enero";
        } else if (mes == 2 && idioma == "es") {
            mesescrito = "Febrero";
        } else if (mes == 3 && idioma == "es") {
            mesescrito = "Marzo";
        } else if (mes == 4 && idioma == "es") {
            mesescrito = "Abril";
        } else if (mes == 5 && idioma == "es") {
            mesescrito = "Mayo";
        } else if (mes == 6 && idioma == "es") {
            mesescrito = "Junio";
        } else if (mes == 7 && idioma == "es") {
            mesescrito = "Julio";
        } else if (mes == 8 && idioma == "es") {
            mesescrito = "Agosto";
        } else if (mes == 9 && idioma == "es") {
            mesescrito = "Septiembre";
        } else if (mes == 10 && idioma == "es") {
            mesescrito = "Octubre";
        } else if (mes == 11 && idioma == "es") {
            mesescrito = "Noviembre";
        } else if (mes == 12 && idioma == "es") {
            mesescrito = "Diciembre";
        } else if (mes == 1 && idioma == "en") {
            mesescrito = "January";
        } else if (mes == 2 && idioma == "en") {
            mesescrito = "February";
        } else if (mes == 3 && idioma == "en") {
            mesescrito = "March";
        } else if (mes == 4 && idioma == "en") {
            mesescrito = "April";
        } else if (mes == 5 && idioma == "en") {
            mesescrito = "May";
        } else if (mes == 6 && idioma == "en") {
            mesescrito = "June";
        } else if (mes == 7 && idioma == "en") {
            mesescrito = "July";
        } else if (mes == 8 && idioma == "en") {
            mesescrito = "Agost";
        } else if (mes == 9 && idioma == "en") {
            mesescrito = "September";
        } else if (mes == 10 && idioma == "en") {
            mesescrito = "October";
        } else if (mes == 11 && idioma == "en") {
            mesescrito = "November";
        } else if (mes == 12 && idioma == "en") {
            mesescrito = "Dicember";
        } else if (mes == 1 && idioma == "ca") {
            mesescrito = "Gener";
        } else if (mes == 2 && idioma == "ca") {
            mesescrito = "Febrer";
        } else if (mes == 3 && idioma == "ca") {
            mesescrito = "Març";
        } else if (mes == 4 && idioma == "ca") {
            mesescrito = "Abril";
        } else if (mes == 5 && idioma == "ca") {
            mesescrito = "Maig";
        } else if (mes == 6 && idioma == "ca") {
            mesescrito = "Juny";
        } else if (mes == 7 && idioma == "ca") {
            mesescrito = "Juliol";
        } else if (mes == 8 && idioma == "ca") {
            mesescrito = "Agost";
        } else if (mes == 9 && idioma == "ca") {
            mesescrito = "Setembre";
        } else if (mes == 10 && idioma == "ca") {
            mesescrito = "Octubre";
        } else if (mes == 11 && idioma == "ca") {
            mesescrito = "Novembre";
        } else {
            mesescrito = "Desembre";
        }
        return mesescrito;
    }

    static String[] semana_es = { "lunes", "martes", "miercoles", "jueves", "viernes", "sabado", "domingo" };
    static String[] semana_en = { "monday ", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday" };
    static String[] semana_ca = { "Dilluns", "dimarts", "dimecres", "dijous", "divendres", "dissabte", "diumenge" };

    public static String Semanas(int semana, String idioma) {
        String semescrito = "";
        if (idioma == "es") {
            semescrito = semana_es[semana - 1];
        } else if (idioma == "en") {
            semescrito = semana_en[semana - 1];
        } else {
            semescrito = semana_ca[semana - 1];
        }
        return semescrito;
    }

    public static int Mayorint(int[] numeros) {
        int i = 0;
        int num = 0;
        do {
            // intenta hacer esto
            if (i == 0) {
                num = numeros[i];
            } else {
                if (numeros[i] > num) {
                    num = numeros[i];
                }
            }
            i++;
        } while (i < numeros.length);
        return num;
    }

    public static char Mayorchar(char[] letras) {
        int i = 0;
        char charmayor = 0;
        do {
            // intenta hacer esto
            if (i == 0) {
                charmayor = letras[i];
            } else {
                if (letras[i] > charmayor) {
                    charmayor = letras[i];
                }
            }
            i++;
        } while (i < letras.length);
        return charmayor;
    }

    public static void Muestraarray(int[] aray, String intercalar) {
        System.out.print("El array intercalado con el string es: ");
        for (int i = 0; i < aray.length; i++) {
            if (i != aray.length - 1) {
                System.out.print(aray[i] + intercalar);
            } else {
                System.out.println(aray[i]);
            }
        }
    }

    public static int[] Muestrapares(int[] pares) {
        int e = 0;
        int u = 0;
        int u2 = 0;
        for (e = 0; e < pares.length; e++) {
            if (pares[e] % 2 == 0) {
                u2++;
            }
        }
        int[] cuentapares = new int[u2];
        for (int i = 0; i < pares.length; i++) {
            if (pares[i] % 2 == 0) {
                cuentapares[u] = pares[i];
                u++;
            }
        }
        return cuentapares;
    }

    public static String Password(int ingresacontrasena) {
        String contrasena = "";
        Random random = new Random();

        char[] letrasynum = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
                's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
                'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7',
                '8', '9' };
        char[] letrasmayusculas = {};
        char[] todoslosnumeros = {};
        for (int i = 0; i < ingresacontrasena; i++) {
            int incognita = random.nextInt(58) + 1;
            contrasena = contrasena + letrasynum[incognita];
        }

        if (Util.Verificapassword(contrasena) == true) {
            return contrasena;
        } else {
            System.out.println(contrasena);
            return Util.Password(ingresacontrasena);

        }
    }

    public static boolean Verificaemail(String email) {
        // boolean compruebaemail = false;
         return (email.matches(".*...@(gmail|hotmail).(com|es)$"));
        //  {
        // compruebaemail=true;
        // }
        // {
        // compruebaemail=true;
        // }
        // return compruebaemail;
    }

    public static boolean Verificapassword(String password) {
        boolean compruebapassword = false;
        if (password.length() > 8 && password.matches(".*[A-Z].*") && password.matches(".*[a-z].*")
                && password.matches(".*[1-9].*")) {
            compruebapassword = true;
        }
        return compruebapassword;

    }
}
