import java.util.Arrays;

class Testutil {

    public static void main(String[] args) {
        int semana= 5;
        int mes= 12;
        String idioma= "ca";
        char[] letras= {'z','b','c','d','e'};
        int[] numeros ={10,5,9,20,99};
        int [] aray ={10,5,6,8,55,98};
        String intercalar =">>>";
        int []pares = {20,13,88,12,4,38,15};
        int ingresacontrasena=10;
        String email="miguelangelmahiamasip@gmail.com";
        System.out.println("El dia de la semana ingresado es: "+Util.Semanas(semana,idioma));
        System.out.println("El mes ingresado es: "+Util.Meses(mes,idioma));
        System.out.println("El numero mayor igresado es: "+Util.Mayorint(numeros));
        System.out.println("El char mayor igresado es: "+Util.Mayorchar(letras));
        Util.Muestraarray(aray,intercalar);
        System.out.println("Los numeros pares del array son: "+Arrays.toString(Util.Muestrapares(pares)));
        System.out.println("La contrasena secreta es: "+Util.Password(ingresacontrasena));
        System.out.println("El email ingresado es: "+Util.Verificaemail(email));
        //System.out.println("El la contrasena generada es: "+Util.Verificapassword(Util.Password(ingresacontrasena)));

    }
}