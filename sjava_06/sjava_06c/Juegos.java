import java.util.Random;
import java.util.Scanner;

import javax.lang.model.util.ElementScanner6;

public class Juegos {

    public static Jugador jugador1;
    public static Jugador jugador2;
    public static int partidas;
    public static Scanner keyboard = new Scanner(System.in);

    /**
     * pide nombre de jugador, 1 o 2 crea objeto "Jugador" y lo asigna a jugador1 o
     * jugador2
     */
    public static void pideJugador(int numJugador) {
        // pedimos nombre de jugador
        String pregunta = String.format("Nombre para el jugador %d: ", numJugador);
        System.out.println(pregunta);
        String nombre = keyboard.next();
        Jugador j = new Jugador(nombre);

        if (numJugador == 1) {
            Juegos.jugador1 = j;
        } else {
            Juegos.jugador2 = j;
        }
    }

    /**
     * muestra menu de juegos y pide opción
     */
    public static void menu() {
        System.out.println("*******************");
        System.out.println("JUEGOS DISPONIBLES:");
        System.out.println("*******************");
        System.out.println("1: Cara o Cruz");
        System.out.println("2: Piedra papel o tijera");
        System.out.println("3: La rutela ");
        System.out.println("********************");
        // pedimos opcion de juego, comprobando validez
        int opcion = 0;
        do {
            System.out.print("Introduce juego: ");
            opcion = keyboard.nextInt();
        } while (opcion < 1 || opcion > 3);

        switch (opcion) {
        case 1:
            Juegos.caraCruz();
            break;
        case 2:
            Juegos.piedra_papel_tijera();
            break;
        case 3:
            Juegos.ruleta();
            break;
        default:
            break;
        }
    }

    /**
     * juego caraCruz
     */
    public static void caraCruz() {
        Juegos.pideJugador(1); // solo interviene un jugador en este juego
        Juegos.partidas = 5; // partidas por defecto en este juego

        System.out.println();
        System.out.println("CARA O CRUZ:");
        System.out.println("************");
        System.out.printf("Bienvenido %s!", Juegos.jugador1.nombre);
        System.out.println();
        System.out.printf("Vamos a jugar %d partidas", Juegos.partidas);
        System.out.println();

        Random rnd = new Random();
        // bucle de partidas
        for (int i = 0; i < Juegos.partidas; i++) {
            System.out.print("Cara (C) o cruz(X) ?  ");
            // pedimos apuesta aunque no se utiliza
            String apuesta = keyboard.next();
            // sea cual sea la probabilidad es un 50%...
            // obtenemos un boolean aleatorio
            boolean ganador = rnd.nextBoolean();
            if (ganador) {
                System.out.println(" Has acertado!");
                Juegos.jugador1.ganadas++;
            } else {
                System.out.println(" Lo siento...");
            }
            Juegos.jugador1.partidas++;
        }
        // creamos string con el resumen final de juego y lo mostramos
        String resumen = String.format("Jugador %s, %d partidas, %d ganadas.", Juegos.jugador1.nombre,
                Juegos.jugador1.partidas, Juegos.jugador1.ganadas);
        System.out.println(resumen);
    }

    public static void piedra_papel_tijera() {
        Juegos.pideJugador(1); // solo interviene un jugador en este juego
        Juegos.partidas = 5; // partidas por defecto en este juego

        System.out.println();
        System.out.println("Piedra, papel, tijera:");
        System.out.println("**********************");
        System.out.printf("Bienvenido %s!", Juegos.jugador1.nombre);
        System.out.println();
        System.out.printf("Vamos a jugar %d partidas", Juegos.partidas);
        System.out.println();

        Random random = new Random();
        // bucle de partidas
        int[] arraypiedrapapeltijera = { 1, 2, 3 };
        for (int i = 0; i < Juegos.partidas; i++) {
            System.out.print("Piedra (1), Papel (2) o Tijera (3): ");
            int incognita = random.nextInt(3) + 1;
            // pedimos apuesta aunque no se utiliza
            int apuesta = keyboard.nextInt();
            // sea cual sea la probabilidad es un 50%...
            // obtenemos un boolean aleatorio
            if (apuesta == incognita) {
                System.out.println("Yo tambien he puesto "+incognita+ " .Empate");
                Juegos.jugador1.empates++;
            } else if ((apuesta == 1 && incognita == 3) || (apuesta == 2 && incognita == 1)
                    || (apuesta == 3 && incognita == 2)) {
                System.out.println("Te odio!! yo he puesto "+incognita+" Me has ganado");
                Juegos.jugador1.ganadas++;
            } else {
                System.out.println(" ja ja ja .Yo he puesto "+incognita+" te gane!");
            }

            Juegos.jugador1.partidas++;
        }

        // creamos string con el resumen final de juego y lo mostramos
        String resumen = String.format("Jugador %s, %d partidas , %d empates, %d ganadas.", Juegos.jugador1.nombre,
                Juegos.jugador1.partidas, Juegos.jugador1.empates, Juegos.jugador1.ganadas);
        System.out.println(resumen);
    }

    public static void ruleta() {
        Juegos.pideJugador(1); // solo interviene un jugador en este juego
        Juegos.partidas = 5; // partidas por defecto en este juego

        System.out.println();
        System.out.println("Rutela:");
        System.out.println("******");
        System.out.printf("Bienvenido %s!", Juegos.jugador1.nombre);
        System.out.println();
        System.out.printf("Vamos a jugar %d partidas", Juegos.partidas);
        System.out.println();

        // bucle de partidas
        Random rnd = new Random();

        int dinero = 100;
        int contador = dinero;
        System.out.print("Tienes " + dinero + " euros. Cuanto apuestas al Rojo o negro? ");
        int apuesta = keyboard.nextInt();
        // sea cual sea la probabilidad es un 50%...
        // obtenemos un boolean aleatorio
        if (apuesta > 0) {
            for (int i = 0; i < 1; i++) {
                System.out.print("Rojo(r) o Negro(n)?");
                // pedimos apuesta aunque no se utiliza
                String apuesta2 = keyboard.next();

                // sea cual sea la probabilidad es un 50%...
                // obtenemos un boolean aleatorio
                boolean ganador = rnd.nextBoolean();
                if (ganador) {
                    System.out.println(" Has acertado!");
                    Juegos.jugador1.ganadas++;
                    contador = contador + apuesta;
                } else {
                    System.out.println(" Lo siento...");
                    contador = contador - apuesta;
                }
                Juegos.jugador1.partidas++;
            }
            // creamos string con el resumen final de juego y lo mostramos
            String resumen = String.format(
                    "Jugador %s, %d partidas , %d ganadas, y ahora tienes " + contador + "euros.",
                    Juegos.jugador1.nombre, Juegos.jugador1.partidas, Juegos.jugador1.ganadas);
            System.out.println(resumen);
        } else {
            System.out.print("Tienes " + dinero + " euros. Cuanto apuestas alpar o impar? ");
            int apuesta3 = keyboard.nextInt();
            if (apuesta3 > 0) {
                for (int i = 0; i < 1; i++) {
                    System.out.print("Par(p) o impar(i)?");
                    // pedimos apuesta aunque no se utiliza
                    String apuesta4 = keyboard.next();
                    // sea cual sea la probabilidad es un 50%...
                    // obtenemos un boolean aleatorio
                    boolean ganador = rnd.nextBoolean();
                    if (ganador) {
                        System.out.println(" Has acertado!");
                        Juegos.jugador1.ganadas++;
                        contador = contador + apuesta3;
                    } else {
                        System.out.println(" Lo siento...");
                        contador = contador - apuesta3;
                    }
                    Juegos.jugador1.partidas++;
                }
                // creamos string con el resumen final de juego y lo mostramos
                String resumen = String.format(
                        "Jugador %s, %d partidas , %d ganadas, y ahora tienes " + contador + "euros.",
                        Juegos.jugador1.nombre, Juegos.jugador1.partidas, Juegos.jugador1.ganadas);
                System.out.println(resumen);
            } else {
                System.out.print("Tienes " + dinero + " euros. Cuanto apuestas Falta o Pasa? ");
                int apuesta5 = keyboard.nextInt();
                if (apuesta5 > 0) {
                    for (int i = 0; i < 1; i++) {
                        System.out.print("Falta (f) o Pasa (p)");
                        // pedimos apuesta aunque no se utiliza
                        String apuesta6 = keyboard.next();
                        // sea cual sea la probabilidad es un 50%...
                        // obtenemos un boolean aleatorio
                        boolean ganador = rnd.nextBoolean();
                        if (ganador) {
                            System.out.println(" Has acertado!");
                            Juegos.jugador1.ganadas++;
                            contador = contador + apuesta5;
                        } else {
                            System.out.println(" Lo siento...");
                            contador = contador - apuesta5;
                        }
                        Juegos.jugador1.partidas++;
                    }
                    // creamos string con el resumen final de juego y lo mostramos
                    String resumen = String.format(
                            "Jugador %s, %d partidas , %d ganadas, y ahora tienes " + contador + "euros.",
                            Juegos.jugador1.nombre, Juegos.jugador1.partidas, Juegos.jugador1.ganadas);
                    System.out.println(resumen);
                }
                else {System.out.println("Parece que no quieres jugar...");}
            }
        }
    }
}




//     public static void ruleta() {
//         Juegos.pideJugador(1); // solo interviene un jugador en este juego
//         Juegos.partidas = 5; // partidas por defecto en este juego

//         System.out.println();
//         System.out.println("Rutela:");
//         System.out.println("******");
//         System.out.printf("Bienvenido %s!", Juegos.jugador1.nombre);
//         System.out.println();
//         System.out.printf("Vamos a jugar %d partidas", Juegos.partidas);
//         System.out.println();

//         // bucle de partidas
//         Random rnd = new Random();

//         int dinero = 100;
//         int contador = dinero;
//         System.out.print("Tienes " + dinero + " euros. Cuanto apuestas al Rojo o negro? ");
//         int apuesta = keyboard.nextInt();
//         // sea cual sea la probabilidad es un 50%...
//         // obtenemos un boolean aleatorio
//         if (apuesta > 0) {
//             for (int i = 0; i < 1; i++) {
//                 System.out.print("Rojo(1) o Negro(2)?");
//                 // pedimos apuesta aunque no se utiliza
//                 String apuesta2 = keyboard.next();

//                 int incognita2 = random.nextInt(2) + 1;
//                 // pedimos apuesta aunque no se utiliza
//                 int apuesta3 = keyboard.nextInt();
//                 if (apuesta3 == incognita){
//                     contador= contador + apuesta;
//                 } else{ contador = contador - apuesta;}
//                 System.out.print("Cuanto apuestas al par o impar? ");
//                 int apuesta11 = keyboard.nextInt();
//                 System.out.print("Ingrese el numero par o impar: ");
//                 // pedimos apuesta aunque no se utiliza
//                 int apuesta4 = keyboard.next();

//                 int incognita3 = random.nextInt(36) + 1;
//                 // pedimos apuesta aunque no se utiliza
//                 if ((apuesta4 %2==0 &&  incognita3%2==0)||(apuesta4 %2>0 &&  incognita3%2>0)){
//                     contador= contador + apuesta;
//                 } else{ contador = contador - apuesta;}


//         //         // sea cual sea la probabilidad es un 50%...
//         //         // obtenemos un boolean aleatorio
//         //         boolean ganador = rnd.nextBoolean();
//         //         if (ganador) {
//         //             System.out.println(" Has acertado!");
//         //             Juegos.jugador1.ganadas++;
//         //             contador = contador + apuesta;
//         //         } else {
//         //             System.out.println(" Lo siento...");
//         //             contador = contador - apuesta;
//         //         }
//         //         Juegos.jugador1.partidas++;
//         //     }
//         //     // creamos string con el resumen final de juego y lo mostramos
//         //     String resumen = String.format(
//         //             "Jugador %s, %d partidas , %d ganadas, y ahora tienes " + contador + "euros.",
//         //             Juegos.jugador1.nombre, Juegos.jugador1.partidas, Juegos.jugador1.ganadas);
//         //     System.out.println(resumen);
//         // } else {
//         //     System.out.print("Tienes " + dinero + " euros. Cuanto apuestas alpar o impar? ");
//         //     int apuesta3 = keyboard.nextInt();
//         //     if (apuesta3 > 0) {
//         //         for (int i = 0; i < 1; i++) {
//         //             System.out.print("Par(p) o impar(i)?");
//         //             // pedimos apuesta aunque no se utiliza
//         //             String apuesta4 = keyboard.next();
//         //             // sea cual sea la probabilidad es un 50%...
//         //             // obtenemos un boolean aleatorio
//         //             boolean ganador = rnd.nextBoolean();
//         //             if (ganador) {
//         //                 System.out.println(" Has acertado!");
//         //                 Juegos.jugador1.ganadas++;
//         //                 contador = contador + apuesta3;
//         //             } else {
//         //                 System.out.println(" Lo siento...");
//         //                 contador = contador - apuesta3;
//         //             }
//         //             Juegos.jugador1.partidas++;
//         //         }
//         //         // creamos string con el resumen final de juego y lo mostramos
//         //         String resumen = String.format(
//         //                 "Jugador %s, %d partidas , %d ganadas, y ahora tienes " + contador + "euros.",
//         //                 Juegos.jugador1.nombre, Juegos.jugador1.partidas, Juegos.jugador1.ganadas);
//         //         System.out.println(resumen);
//         //     } else {
//         //         System.out.print("Tienes " + dinero + " euros. Cuanto apuestas Falta o Pasa? ");
//         //         int apuesta5 = keyboard.nextInt();
//         //         if (apuesta5 > 0) {
//         //             for (int i = 0; i < 1; i++) {
//         //                 System.out.print("Falta (f) o Pasa (p)");
//         //                 // pedimos apuesta aunque no se utiliza
//         //                 String apuesta6 = keyboard.next();
//         //                 // sea cual sea la probabilidad es un 50%...
//         //                 // obtenemos un boolean aleatorio
//         //                 boolean ganador = rnd.nextBoolean();
//         //                 if (ganador) {
//         //                     System.out.println(" Has acertado!");
//         //                     Juegos.jugador1.ganadas++;
//         //                     contador = contador + apuesta5;
//         //                 } else {
//         //                     System.out.println(" Lo siento...");
//         //                     contador = contador - apuesta5;
//         //                 }
//         //                 Juegos.jugador1.partidas++;
//         //             }
//         //             // creamos string con el resumen final de juego y lo mostramos
//         //             String resumen = String.format(
//         //                     "Jugador %s, %d partidas , %d ganadas, y ahora tienes " + contador + "euros.",
//         //                     Juegos.jugador1.nombre, Juegos.jugador1.partidas, Juegos.jugador1.ganadas);
//         //             System.out.println(resumen);
//         //         }
//         //         else {System.out.println("Parece que no quieres jugar...");}
//         //     }
//         }
//     }
// System.out.println("Yo he elegido "+incognita2+" y "+incognita3 +" tienes  euros");


//     }
// }
