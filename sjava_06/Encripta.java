public class Encripta {

    public static void encripta1(String palabra) {
        // String palabra;
        for (int i = 0; i < palabra.length(); i++) {

            if (palabra.charAt(i) == 'a') {
                System.out.print("1");
            } else if (palabra.charAt(i) == 'e') {
                System.out.print('2');
            } else if (palabra.charAt(i) == 'i') {
                System.out.print("3");
            } else if (palabra.charAt(i) == 'o') {
                System.out.print("4");
            } else if (palabra.charAt(i) == 'u') {
                System.out.print("5");
            } else {
                System.out.print(palabra.charAt(i));
            }
        }
    }
    public static void encripta2(String palabra) {
        // String palabra;
        for (int i = 0; i < palabra.length(); i++) {
            char letra = palabra.charAt(i);
            letra ++;
            System.out.print(letra);
            
        }
    }

    public static void desencripta1(String palabra) {
        // String palabra;
        for (int i = 0; i < palabra.length(); i++) {

            if (palabra.charAt(i) == '1') {
                System.out.print("a");
            } else if (palabra.charAt(i) == '2') {
                System.out.print('e');
            } else if (palabra.charAt(i) == '3') {
                System.out.print("i");
            } else if (palabra.charAt(i) == '4') {
                System.out.print("o");
            } else if (palabra.charAt(i) == '5') {
                System.out.print("u");
            } else {
                System.out.print(palabra.charAt(i));
            }
        }
    }



    public static void desencripta2(String palabra) {
        // String palabra;
        for (int i = 0; i < palabra.length(); i++) {
            char letra = palabra.charAt(i);
            letra--;
            System.out.print(letra);
            
        }
    }
}