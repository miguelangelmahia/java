package com.codesplai.java.miguelangel;

class Test {

    public static void main(String[] args) {
        Moto m1 = new Moto("Honda", "Scoopy", 250, 300, 400);
        Moto m2 = new Moto("Yamaha", "Tenere", 200,150,500);

        System.out.println(m1);
        System.out.println(m2);
        m1.comparaCv(m2);
        m1.comparaPvp(m2);
    }
}