import java.util.Scanner;

class InfoNumsK {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        int total = 0;
        int max = 0;
        int min = 0;
        int i=0;
        int num=1;
        do {
            System.out.println("Entra un num: ");
            //intenta hacer esto
            try {
                num = keyboard.nextInt();
                if (i == 0) {
                    max = num;
                    min = num;
                } else {
                    if (num > max) {
                        max = num;
                    }
                    if (num < min) {
                        min = num;
                    }
                }
                total = total + num;
                i++;
    
            }// si da error haz esto 
            catch (Exception e) {
                System.out.println("Dato Incorrecto- introduzca 0 para salir");
                keyboard.next();
            }
        } while (num != 0);
          
        
        float longitud = args.length;
        float promedio = total / (i-1);
        System.out.println("El total es " + total);
        System.out.println("La cantidad de numeros sumados son " + (i-1));
        System.out.println("El promedio es " + promedio);
        System.out.println("El numero mayor es " + max);
        System.out.println("El numero menor es " + min);
    }}
