import java.util.Scanner;

class PideNums {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        int total = 0;
        int num = 0;
        do {
            System.out.println("Entra un num: ");
            //intenta hacer esto
            try {
                num = keyboard.nextInt();
                total += num;
            }// si de error haz esto 
            catch (Exception e) {
                System.out.println("Dato Incorrecto- introduzca 0 para salir");
                keyboard.next();
            }
        } while (num != 0);

        System.out.println("La suma es " + total);
        keyboard.close();
    }
}