class SumaNums {
    public static void main(String[] args) {
        int total=0;
        for (String s : args){
            //interger.parseInt(s) acepata el string y lo convierte en un int
            int num = Integer.parseInt(s);
            total += num;
        }
        System.out.println("El total es "+total);
    }
}
