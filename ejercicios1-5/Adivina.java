import java.util.Scanner;
import java.util.Random;

class Adivina {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        int num = 0;
        int i=0;
        Random random = new Random();
        int incognita = random.nextInt(100) + 1;

        System.out.println("Adivina el numero secreto: ");
        do {
            // intenta hacer esto
            
            try {
                num = keyboard.nextInt();
                i++;
                if(num > incognita){System.out.println("Te pasaste, intenta con un numero menor");}
                else if(num < incognita){System.out.println("Te quedaste corto, intenta con un numero mayor");}
            } // si de error haz esto
            catch (Exception e) {
                System.out.println("Dato Incorrecto");
                keyboard.next();
            }
        } while (num != incognita);

        System.out.println("Lo conseguiste!!, Tardaste "+i+ " intentos");
        keyboard.close();
    }
}